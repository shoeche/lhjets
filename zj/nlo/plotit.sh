#!/bin/bash

rivet-mkhtml -c ../plots.conf \
    -o ~/public_html/pub/lh17/zj/fo/nlo \
    cs/run.yoda:Title=Sherpa~NLO \
    herwig.yoda:Title=Herwig~NLO
