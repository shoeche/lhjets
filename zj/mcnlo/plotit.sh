#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/zj/ps/nlo \
    css/run.yoda:Title=Sherpa~MCNLO \
    css-1em/run.yoda:Title=Sherpa~MCNLO~1em \
    herwig.yoda:Title=Herwig~MCNLO \
    ../nlo/cs/run.yoda:Title=Sherpa~NLO:ErrorBars=0
