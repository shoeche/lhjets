#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/zj/ps/lo \
    css/run.yoda:Title=Sherpa~PS \
    css-1em/run.yoda:Title=Sherpa~PS~1em \
    herwig.yoda:Title=Herwig~PS \
    ../lo/cs/run.yoda:Title=Sherpa~LO \
