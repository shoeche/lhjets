// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/JetShape.hh"

namespace Rivet {


  /// @brief ATLAS jet shape analysis
  /// @author Andy Buckley, Judith Katzy, Francesc Vives
  class LH2017_ZHSHAPES : public Analysis {
  public:

    /// Constructor
    LH2017_ZHSHAPES()
      : Analysis("LH2017_ZHSHAPES")
    {    }


    /// @name Analysis methods
    //@{

    void init() {
      // Set up projections
      const FinalState fs(-5.0, 5.0);
      declare(fs, "FS");
      VetoedFinalState hfs(fs);
      hfs.addVetoId(25);
      hfs.addVetoId(23);
      hfs.addVetoId(11);
      hfs.addVetoId(-11);

      // Specify pT bins
      _ptedges = {{ 50.0, 100.0, 200.0, 400.0, 800.0, 1600.0, 3200.0 }};
      _yedges  = {{ 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1 }};

      // Register a jet shape projection and histogram for each pT bin
      for (size_t j = 0; j < 8; ++j) {
	FastJets fj(hfs, FastJets::ANTIKT, _yedges[j]);
	fj.useInvisibles();
	declare(fj, "Jets"+to_str(_yedges[j]));
	for (size_t i = 0; i < 6; ++i) {

          // Set up projections for each (pT,y) bin
          _jsnames_pT[i][j] = "JetShape" + to_str(i) + to_str(j);
          const JetShape jsp(fj, 0.0, 1.5, 150, _ptedges[i], _ptedges[i+1], -4.5, 4.5, RAPIDITY);
          declare(jsp, _jsnames_pT[i][j]);

          // Book profile histograms for each (pT,y) bin
          _profhistRho_pT[i][j] = bookProfile1D("rho_" + to_str(_yedges[j]) +"_"+ to_str(_ptedges[i]), 150, 0, 1.5);
          _profhistPsi_pT[i][j] = bookProfile1D("psi_" + to_str(_yedges[j]) +"_"+ to_str(_ptedges[i]), 150, 0, 1.5);
        }
      }
    }



    /// Do the analysis
    void analyze(const Event& evt) {

      // Calculate and histogram jet shapes
      const double weight = evt.weight();

      for (size_t jy = 0; jy < 8; ++jy) {
	// Get jets and require at least one to pass pT and y cuts
	const Jets jets = apply<FastJets>(evt, "Jets"+to_str(_yedges[jy]))
	  .jetsByPt(Cuts::ptIn(_ptedges.front()*GeV, _ptedges.back()*GeV) && Cuts::absrap < 4.5);
	MSG_DEBUG("Jet multiplicity before cuts = " << jets.size());
	if (jets.size() == 0) continue;
	for (size_t ipt = 0; ipt < 6; ++ipt) {
          const JetShape& jsipt = apply<JetShape>(evt, _jsnames_pT[ipt][jy]);
          for (size_t ijet = 0; ijet < jsipt.numJets(); ++ijet) {
            for (size_t rbin = 0; rbin < jsipt.numBins(); ++rbin) {
              const double r_rho = jsipt.rBinMid(rbin);
              _profhistRho_pT[ipt][jy]->fill(r_rho, (1./0.1)*jsipt.diffJetShape(ijet, rbin), weight);
              const double r_Psi = jsipt.rBinMid(rbin);
              _profhistPsi_pT[ipt][jy]->fill(r_Psi, jsipt.intJetShape(ijet, rbin), weight);
            }
          }
        }
      }
    }


    // Finalize
    void finalize() {
    }

    //@}


  private:

    /// @name Analysis data
    //@{

    /// Jet \f$ p_\perp\f$ bins.
    vector<double> _ptedges; // This can't be a raw array if we want to initialise it non-painfully
    vector<double> _yedges;

    /// JetShape projection name for each \f$p_\perp\f$ bin.
    string _jsnames_pT[7][8];

    //@}

    /// @name Histograms
    //@{
    Profile1DPtr _profhistRho_pT[7][8];
    Profile1DPtr _profhistPsi_pT[7][8];
    //@}

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(LH2017_ZHSHAPES);

}
