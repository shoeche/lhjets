#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/zj/fo/lo \
    cs/run.yoda:Title=Sherpa~LO \
    herwig.yoda:Title=Herwig~LO
