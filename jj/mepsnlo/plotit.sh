#!/bin/bash

rivet-mkhtml --mc-errs -c plots.conf \
    -o ~/public_html/pub/lh17/jj/meps/1-2 \
    css-1j/run.yoda:Title=Sherpa~MEPS~(1)1j \
    css-2j/run.yoda:Title=Sherpa~MEPS~(1)2j \
    ../MCNLO/css/run.yoda:Title=Sherpa~MCNLO \
    ../NLO/cs/run.yoda:Title=NLO:ErrorBars=0
