#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/jj/ps/nlo \
    css/run.yoda:Title=Sherpa~MCNLO \
    css-1em/run.yoda:Title=Sherpa~MCNLO~1em \
    herwig.yoda:Title=Herwig~MCNLO:ErrorBars=0 \
    powheg.yoda:Title=POWHEG \
    ../nlo/cs/run.yoda:Title=NLO:ErrorBars=0
