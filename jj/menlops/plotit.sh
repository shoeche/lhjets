#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/jj/meps/0-2 \
    css/run.yoda:Title=Sherpa~MEPS~\(0\)2j \
    ../mcnlo/css/run.yoda:Title=Sherpa~MCNLO \
    ../nlo/cs/run.yoda:Title=NLO:ErrorBars=0
