
import sys, os, shutil, stat, optparse, itertools, inspect

def perms(s):
    if len(s) <=1:
        yield s
    else:
        for i in range(len(s)):
            for p in perms(s[:i]+s[i+1:]):
                yield s[i]+p

parser = optparse.OptionParser()
parser.add_option("-r","--resubmit",default=False,action="store_true",dest="resubmit")
parser.add_option("-n","--nruns",default=1,dest="nruns")
parser.add_option("-O","--offset",default=0,dest="offset")
parser.add_option("-o","--options",default=[],dest="opts",action="append")
parser.add_option("-f","--file",default="run",dest="name")
parser.add_option("-b","--binary",default="/nfs/farm/g/theory/qcdsim/shoeche/sherpa/master",dest="bin")
(opts,args) = parser.parse_args()

cwd = os.getcwd()
if opts.resubmit:
    os.chdir('{0}/{1}'.format(cwd,opts.name))
else:
    os.mkdir(opts.name)
    os.chdir(opts.bin)
    os.system('git diff > {0}/{1}/source.patch'.format(cwd,opts.name))
    os.system('cp -r bin/ {0}/{1}/bin'.format(cwd,opts.name))
    os.system('cp -r lib/ {0}/{1}/lib'.format(cwd,opts.name))
    os.chdir('{0}/{1}'.format(cwd,opts.name))
    with open('../'+inspect.stack()[0][1],'r') as source:
        with open('submit.py','w') as target:
            target.write('# '+' '.join(sys.argv)+'\n'+source.read())
    shutil.copyfile('../Run.dat','Run.dat')
    shutil.copyfile('../../rivet/RivetAnalysis.so','RivetAnalysis.so')
    shutil.copyfile('../../rivet/CMS_RAD.yoda','CMS_RAD.yoda')
    shutil.copytree('../Process','Process')
    try:
        shutil.copyfile('../Results.db','Results.db')
    except IOError:
        pass
n = 0
for s in [i for i in perms('123456')] \
        +[i for i in perms('234567')] \
        +[i for i in perms('345678')] \
        +[i for i in perms('456789')] \
        +[i for i in perms('567891')] \
        +[i for i in perms('678912')] \
        +[i for i in perms('789123')] \
        +[i for i in perms('891234')] \
        +[i for i in perms('912345')]:
    n += 1
    if n < int(opts.offset): continue
    if n > int(opts.offset)+int(opts.nruns): break
    cmd = "bsub -e {0}/{1}/lsf_{2}.log -o {0}/{1}/lsf_{2}.log -R 'select[bullet]' -W 5:59".format(cwd,opts.name,s)
    cmd += ' LD_LIBRARY_PATH={0}/{1}/lib/SHERPA-MC:$LD_LIBRARY_PATH LD_DEBUG=libs'.format(cwd,opts.name)
    cmd += ' ./bin/Sherpa '.format(cwd,opts.name)+' '.join(opts.opts)
    cmd += ' -R{0} -Arun_{0}/ -lrun_{0}.log\n'.format(s)
    os.system(cmd)
