#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/hj/meps/1-3 \
    css-1j/run.yoda:Title=Sherpa~MEPS~\(1\)1j \
    css-2j/run.yoda:Title=Sherpa~MEPS~\(1\)2j \
    css-3j/run.yoda:Title=Sherpa~MEPS~\(1\)3j \
    ../mcnlo/css/run.yoda:Title=Sherpa~MCNLO \
    ../nlo/cs/run.yoda:Title=Sherpa~NLO:ErrorBars=0
