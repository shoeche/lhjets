#!/bin/bash

rivet-mkhtml --mc-errs -c ../plots.conf \
    -o ~/public_html/pub/lh17/hj/meps/0-3 \
    css/run.yoda:Title=Sherpa~MEPS~\(0\)3j \
    ../mcnlo/css/run.yoda:Title=Sherpa~MCNLO \
    ../nlo/cs/run.yoda:Title=Sherpa~NLO:ErrorBars=0
