// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/JetShape.hh"

namespace Rivet {

  class JetShapeLog : public Projection {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor from histo range and number of bins.
    JetShapeLog(const JetAlg& jetalg,
             double rmin, double rmax, size_t nbins,
             double ptmin=0, double ptmax=MAXDOUBLE,
             double absrapmin=-MAXDOUBLE, double absrapmax=-MAXDOUBLE,
             RapScheme rapscheme=RAPIDITY);

    /// Constructor from vector of bin edges.
    JetShapeLog(const JetAlg& jetalg, vector<double> binedges,
             double ptmin=0, double ptmax=MAXDOUBLE,
             double absrapmin=-MAXDOUBLE, double absrapmax=-MAXDOUBLE,
             RapScheme rapscheme=RAPIDITY);

    /// Clone on the heap.
    DEFAULT_RIVET_PROJ_CLONE(JetShapeLog);

    //@}


    /// Reset projection between events.
    void clear();


    /// Do the calculation directly on a supplied collection of Jet objects.
    void calc(const Jets& jets);


  public:


    /// Number of equidistant radius bins.
    size_t numBins() const {
      return _binedges.size() - 1;
    }

    /// Number of jets which passed cuts.
    size_t numJets() const {
      return _diffjetshapes.size();
    }

    /// \f$ r_\text{min} \f$ value.
    double rMin() const {
      return _binedges.front();
    }

    /// \f$ r_\text{max} \f$ value.
    double rMax() const {
      return _binedges.back();
    }

    /// \f$ p_\perp^\text{min} \f$ value.
    double ptMin() const {
      return _ptcuts.first;
    }

    /// \f$ p_\perp^\text{max} \f$ value.
    double ptMax() const {
      return _ptcuts.second;
    }

    /// Central \f$ r \f$ value for bin @a rbin.
    double rBinMin(size_t rbin) const {
      assert(inRange(rbin, 0u, numBins()));
      return _binedges[rbin];
    }

    /// Central \f$ r \f$ value for bin @a rbin.
    double rBinMax(size_t rbin) const {
      assert(inRange(rbin, 0u, numBins()));
      return _binedges[rbin+1];
    }

    /// Central \f$ r \f$ value for bin @a rbin.
    double rBinMid(size_t rbin) const {
      assert(inRange(rbin, 0u, numBins()));
      //cout << _binedges << endl;
      return (_binedges[rbin] + _binedges[rbin+1])/2.0;
    }

    /// Return value of differential jet shape profile histo bin.
    double diffJetShapeLog(size_t ijet, size_t rbin) const {
      assert(inRange(ijet, 0u, numJets()));
      assert(inRange(rbin, 0u, numBins()));
      return _diffjetshapes[ijet][rbin];
    }

    /// Return value of integrated jet shape profile histo bin.
    double intJetShapeLog(size_t ijet, size_t rbin) const {
      assert(inRange(ijet, 0u, numJets()));
      assert(inRange(rbin, 0u, numBins()));
      double rtn  = 0;
      for (size_t i = 0; i <= rbin; ++i) {
        rtn += _diffjetshapes[ijet][i];
      }
      return rtn;
    }

    /// @todo Provide int and diff jet shapes with some sort of area normalisation?

    // /// Return value of \f$ \Psi \f$ (integrated jet shape) at given radius for a \f$ p_T \f$ bin.
    // /// @todo Remove this external indexing thing
    // double psi(size_t pTbin) const {
    //   return _PsiSlot[pTbin];
    // }


  protected:

    /// Apply the projection to the event.
    void project(const Event& e);

    /// Compare projections.
    int compare(const Projection& p) const;


  private:

    /// @name Jet shape parameters
    //@{

    /// Vector of radius bin edges
    vector<double> _binedges;

    /// Lower and upper cuts on contributing jet \f$ p_\perp \f$.
    pair<double, double> _ptcuts;

    /// Lower and upper cuts on contributing jet (pseudo)rapidity.
    pair<double, double> _rapcuts;

    /// Rapidity scheme
    RapScheme _rapscheme;

    //@}


    /// @name The projected jet shapes
    //@{

    /// Jet shape histo -- first index is jet number, second is r bin
    vector< vector<double> > _diffjetshapes;

    //@}

  };

  // Constructor.
  JetShapeLog::JetShapeLog(const JetAlg& jetalg,
                     double rmin, double rmax, size_t nbins,
                     double ptmin, double ptmax,
                     double absrapmin, double absrapmax,
                     RapScheme rapscheme)
    : _rapscheme(rapscheme)
  {
    setName("JetShapeLog");
    _binedges = logspace(nbins, rmin, rmax);
    _ptcuts = make_pair(ptmin, ptmax);
    _rapcuts = make_pair(absrapmin, absrapmax);
    addProjection(jetalg, "Jets");
  }


  // Constructor.
  JetShapeLog::JetShapeLog(const JetAlg& jetalg,
                     vector<double> binedges,
                     double ptmin, double ptmax,
                     double absrapmin, double absrapmax,
                     RapScheme rapscheme)
    : _binedges(binedges), _rapscheme(rapscheme)
  {
    setName("JetShapeLog");
    _ptcuts = make_pair(ptmin, ptmax);
    _rapcuts = make_pair(absrapmin, absrapmax);
    addProjection(jetalg, "Jets");
  }


  int JetShapeLog::compare(const Projection& p) const {
    const int jcmp = mkNamedPCmp(p, "Jets");
    if (jcmp != EQUIVALENT) return jcmp;
    const JetShapeLog& other = pcast<JetShapeLog>(p);
    const int ptcmp = cmp(ptMin(), other.ptMin()) || cmp(ptMax(), other.ptMax());
    if (ptcmp != EQUIVALENT) return ptcmp;
    const int rapcmp = cmp(_rapcuts.first, other._rapcuts.first) || cmp(_rapcuts.second, other._rapcuts.second);
    if (rapcmp != EQUIVALENT) return rapcmp;
    int bincmp = cmp(numBins(), other.numBins());
    if (bincmp != EQUIVALENT) return bincmp;
    for (size_t i = 0; i < _binedges.size(); ++i) {
      bincmp = cmp(_binedges[i], other._binedges[i]);
      if (bincmp != EQUIVALENT) return bincmp;
    }
    return EQUIVALENT;
  }


  void JetShapeLog::clear() {
    _diffjetshapes.clear();
  }


  void JetShapeLog::calc(const Jets& jets) {
    clear();

    foreach (const Jet& j, jets) {
      // Apply jet cuts
      const FourMomentum& pj = j.momentum();
      MSG_DEBUG("Jet pT "<<pj.pT()<<" "<<_ptcuts);
      if (!inRange(pj.pT(), _ptcuts)) continue;
      /// @todo Use Cut for better eta/y selection
      if (_rapscheme == PSEUDORAPIDITY && !inRange(fabs(pj.eta()), _rapcuts)) continue;
      if (_rapscheme == RAPIDITY && !inRange(fabs(pj.rapidity()), _rapcuts)) continue;

      // Fill bins
      vector<double> bins(numBins(), 0.0);
      foreach (const Particle& p, j.particles()) {
        const double dR = deltaR(pj, p.momentum(), _rapscheme);
        const int dRindex = binIndex(dR, _binedges);
        if (dRindex == -1) continue; ///< Out of histo range
        bins[dRindex] += p.pT();
      }

      // Add bin vector for this jet to the diffjetshapes container
      _diffjetshapes += bins;
    }

    // Normalize to total pT
    foreach (vector<double>& binsref, _diffjetshapes) {
      double integral = 0.0;
      for (size_t i = 0; i < numBins(); ++i) {
        integral += binsref[i];
      }
      if (integral > 0) {
        for (size_t i = 0; i < numBins(); ++i) {
          binsref[i] /= integral;
        }
      } else {
        // It's just-about conceivable that a jet would have no particles in the given Delta(r) range...
        MSG_DEBUG("No pT contributions in jet Delta(r) range: weird!");
      }
    }

  }


  void JetShapeLog::project(const Event& e) {
    const Jets jets = applyProjection<JetAlg>(e, "Jets").jets(Cuts::ptIn(_ptcuts.first, _ptcuts.second) &
                                                              ((_rapscheme == PSEUDORAPIDITY) ?
                                                               Cuts::etaIn(-_rapcuts.second, _rapcuts.second) :
                                                               Cuts::rapIn(-_rapcuts.second, _rapcuts.second)) );
    calc(jets);
  }

  /// @brief ATLAS jet shape analysis
  /// @author Andy Buckley, Judith Katzy, Francesc Vives
  class LH2017_ZHSHAPES_LOG : public Analysis {
  public:

    /// Constructor
    LH2017_ZHSHAPES_LOG()
      : Analysis("LH2017_ZHSHAPES_LOG")
    {    }


    /// @name Analysis methods
    //@{

    void init() {
      // Set up projections
      const FinalState fs(-5.0, 5.0);
      declare(fs, "FS");
      VetoedFinalState hfs(fs);
      hfs.addVetoId(25);
      hfs.addVetoId(23);
      hfs.addVetoId(11);
      hfs.addVetoId(-11);

      // Specify pT bins
      _ptedges = {{ 50.0, 100.0, 200.0, 400.0, 800.0, 1600.0, 3200.0 }};
      _yedges  = {{ 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1 }};

      // Register a jet shape projection and histogram for each pT bin
      for (size_t j = 0; j < 8; ++j) {
	FastJets fj(hfs, FastJets::ANTIKT, _yedges[j]);
	fj.useInvisibles();
	declare(fj, "Jets"+to_str(_yedges[j]));
	for (size_t i = 0; i < 6; ++i) {

          // Set up projections for each (pT,y) bin
          _jsnames_pT[i][j] = "JetShapeLog" + to_str(i) + to_str(j);
          const JetShapeLog jsp(fj, 0.001, 10., 100, _ptedges[i], _ptedges[i+1], -4.5, 4.5, RAPIDITY);
          declare(jsp, _jsnames_pT[i][j]);

          // Book profile histograms for each (pT,y) bin
          _profhistRho_pT[i][j] = bookProfile1D("rho_" + to_str(_yedges[j]) +"_"+ to_str(_ptedges[i]), 100, -3., 1.);
          _profhistPsi_pT[i][j] = bookProfile1D("psi_" + to_str(_yedges[j]) +"_"+ to_str(_ptedges[i]), 100, -3., 1.);
        }
      }
    }



    /// Do the analysis
    void analyze(const Event& evt) {

      // Calculate and histogram jet shapes
      const double weight = evt.weight();

      for (size_t jy = 0; jy < 8; ++jy) {
	// Get jets and require at least one to pass pT and y cuts
	const Jets jets = apply<FastJets>(evt, "Jets"+to_str(_yedges[jy]))
	  .jetsByPt(Cuts::ptIn(_ptedges.front()*GeV, _ptedges.back()*GeV) && Cuts::absrap < 4.5);
	MSG_DEBUG("Jet multiplicity before cuts = " << jets.size());
	if (jets.size() == 0) continue;
	for (size_t ipt = 0; ipt < 6; ++ipt) {
          const JetShapeLog& jsipt = apply<JetShapeLog>(evt, _jsnames_pT[ipt][jy]);
          for (size_t ijet = 0; ijet < jsipt.numJets(); ++ijet) {
            for (size_t rbin = 0; rbin < jsipt.numBins(); ++rbin) {
              const double r_rho = jsipt.rBinMid(rbin);
	      if (r_rho>0.)
		_profhistRho_pT[ipt][jy]->fill(log10(r_rho), (1./0.1)*jsipt.diffJetShapeLog(ijet, rbin), weight);
              const double r_Psi = jsipt.rBinMid(rbin);
	      if (r_Psi>0.)
		_profhistPsi_pT[ipt][jy]->fill(log10(r_Psi), jsipt.intJetShapeLog(ijet, rbin), weight);
            }
          }
        }
      }
    }


    // Finalize
    void finalize() {
    }

    //@}


  private:

    /// @name Analysis data
    //@{

    /// Jet \f$ p_\perp\f$ bins.
    vector<double> _ptedges; // This can't be a raw array if we want to initialise it non-painfully
    vector<double> _yedges;

    /// JetShapeLog projection name for each \f$p_\perp\f$ bin.
    string _jsnames_pT[7][8];

    //@}

    /// @name Histograms
    //@{
    Profile1DPtr _profhistRho_pT[7][8];
    Profile1DPtr _profhistPsi_pT[7][8];
    //@}

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(LH2017_ZHSHAPES_LOG);

}
